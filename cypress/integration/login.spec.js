describe("The login page", ()=>{
    beforeEach(() => {
        cy.visit('http://localhost:4200/login');
    });

    it('Should have all form elements', () => {
        cy.get('input[name="userID"]').should('exist');
        cy.get('input[name="password"]').should('exist');
        cy.get('button[type="submit"]').should('exist');
    });

    it('Should submit button disabled', () => {
        cy.get('input[name="userID"]').type('ram.chander');
        cy.get('button[type="submit"]').should('not.have.enabled');
    });

    it('Should submit button enabled', () => {
        cy.get('input[name="userID"]').type('ram.chander');
        cy.get('input[name="password"]').type('123456');
        cy.get('button[type="submit"]').should('not.have.disabled');
    });
})