describe("The Header component", function(){
    beforeEach(() => {        
        cy.visit('http://localhost:4200', {onBeforeLoad: (win) => { win.sessionStorage.clear()}});
    });
    
    
    it('Should route at home page', () => {
        cy.url().should('include', '/home');
    });
    
    it('Should route at home page', () => {
        cy.contains('Home').click();
        cy.url().should('include', '/home');
    });  

    it('Should route at login page', () => {
        cy.contains('Login').click();
        cy.url().should('include', '/login');
    });

    it('Should not route at profile page', () => {
        cy.contains('Profile').click();
        cy.url().should('include', '/home');
    });

    it('Should route at profile page', () => {
        cy.window().then(win => win.sessionStorage.setItem('isLogin','ram'));
        cy.contains('Profile').click();        
        cy.url().should('include', '/profile');
    });
})