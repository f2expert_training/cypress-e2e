describe('The profile page', () =>{
    beforeEach(() => {
        cy.visit('http://localhost:4200/');
        cy.window().then(win => win.sessionStorage.clear());
    });  
    
    it('Should not be route profile', () => {
        cy.url().should('include', '/home');
    });

    it('Should route at profile component', () => {
        cy.window().then(win => win.sessionStorage.setItem('isLogin','ram'));
        cy.visit('http://localhost:4200/profile');
        cy.url().should('include', 'profile');
    });


})