# CypressE2e

## About application functionality
This project has generated with Angular CLI and below are functionalities.

## Header Component

1- It has three nav items.

2- User can navigate the application by clicking the nav.

3- Profile nav is protected, session need to be activated for accessing it.

## Home page

1- only one P tag and containing the page heading

## Login page

1- it has two text boxes and one button.

2- default submit will be disable.

3- user name and password field should not be blank.

4- activate session by clicking the submit button.

5- submit button will be enable after filling the both text boxes

## Profile page  

1- only one P tag and containing the page heading

## installation
Run 	'npm install'

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 8.3.20.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Cypress Server
Run 'npm run cypress'

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).
