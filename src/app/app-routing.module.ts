import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CanComponentDeactivateGuard, CanComponentActivateGuard } from './services/auth.guard';


import { HomeComponent } from './home/home.component';
import { LoginComponent } from './login/login.component';
import { ProfileComponent } from './profile/profile.component';


const routes: Routes = [
  {path:"home", pathMatch:"full", component:HomeComponent},
  {path:"login", pathMatch:"full", component:LoginComponent},
  {path:"profile", pathMatch:"full", component:ProfileComponent, canActivate:[CanComponentActivateGuard]},
  {path:"", pathMatch:"prefix", redirectTo:"/home"}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
